
<?php


class Cercle
{



    private $radius;
    private $point;


    public function __construct($points, $rayon=1)
    {
        $this->point= $points;
        $this->radius = $rayon;
    }


    public function getX()
    {
        return $this->radius;
    }

    public function getY()
    {
        return $this->point;
    }

    public function __toString()
    {
        return "($this->radius,$this->point)";
    }


public function superficie(){
    return pi()*pow($this->radius,2);
}

}

$circle = new Cercle(1000);
?>
 <h1>Test de Cercle</h1>
<div style="width: <?= $circle->getY() ?>; height: <?= $circle->getY() ?>; background-color:crimson ;border-radius: 50%;"></div>
<div style="width: <?=$circle->getRayon()?>px; height: <?=$circle->getRayon()?>px; background-color:crimson;border-radius: 50%; margin-left: <?= $circle->getPoint()->getX()?>px; margin-top: <?= $circle->getPoint()->getY()?>px;"></div>
<p>Sa superficie  : <?= $circle->superficie() ?></p>


