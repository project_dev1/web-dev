<?php


class Point
{

    private $x;
    private $y;


    public function __construct($ax=0, $ay=0)
    {
        $this->x = $ax;
        $this->y = $ay;
    }

    public function __toString()
    {
        return "($this->x,$this->y)";
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function calculateDistance($ax, $ay)
    {
        return sqrt(pow(($ax - $this->x), 2) + pow(($ay - $this->y), 2));
    }

    public function calculatemiddlePoint($ax, $ay)
    {
        return new Point(($ax + $this->x) / 2, ($ay + $this->y) / 2);
    }







}
$p1 = new Point();

 ?>
<h1>Test de Point</h1>
<p>Le point: <?= $p1 ?></p>
<p>Sa composante X : <?= $p1->getX() ?></p>
