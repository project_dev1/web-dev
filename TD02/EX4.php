<?php

$array = array(1, 2, 3);

function map(&$array, $param)
{
    foreach ($array as $key => $value) {
        $array[$key] = $param($value);
    }
}

map($array, function ($elt) {
    return $elt * 2;
});

var_dump($array);
