<?php

function swap(&$a,&$b){
    $c=$a;
    $a=$b;
    $b=$c;
    //on peut faire aussi $a ^= $b ^= $a ^= $b;
    // ou aussi[$a, $b] = [$b, $a];
}

$a=2;
$b=3;
echo "a = $a, b = $b<br>";
swap($a, $b);
echo "a = $a, b = $b<br>";