<?php

function nomMois($mois) {
    $nom = "";

    switch ($mois) {
        case 1:
            $nom = "Janvier";
            break;
        case 2:
            $nom = "Fevrier";
            break;
        case 3:
            $nom = "Mars";
            break;
        case 4:
            $nom = "Avril";
            break;
        case 5:
            $nom = "Mai";
            break;
        case 6:
            $nom = "Juin";
            break;
        case 7:
            $nom = "Juillet";
            break;
        case 8:
            $nom = "Aout";
            break;
        case 9:
            $nom = "Septembre";
            break;
        case 10:
            $nom = "Octobre";
            break;
        case 11:
            $nom = "Novembre";
            break;
        case 12:
            $nom = "Decembre";
            break;
        default:
    }
    return $nom;
}

function afficherTitre($mois,  $année) {
    nomMois($mois);
    if ($mois < 1 || $mois > 12) {
        throw new Exception('Numéro de mois impossible; ' . $mois);

    }
    echo"<tr ><th colspan='7'>";
    echo"==========================";
    echo"       " .nomMois($mois) . " " . $année;
    echo"==========================";
    echo"</th></tr>";


}

function afficherEntête() {
    echo '<tr>';
    echo "<td>Lu</td>  <td>Ma</td> <td>Me</td>  <td>Je</td>  <td>Ve</td>  <td>Sa</td>  <td>Di</td>";
    echo '</tr>';
}

function afficherMois( $décalage, $nombreJours) {
    if ($décalage < 0 || $décalage > 6) {
        throw new Exception('le décalage impossible; ' . $décalage);
    }
    if ($nombreJours < 1 || $nombreJours > 31) {
        throw new Exception('nombre de jour impossible; ' . $nombreJours);

    }

    echo"<tr>";
    for ( $i = 0; $i < $décalage; $i++) {
        echo"<td>  </td>";
    }
    for ( $i = 1; $i <= $nombreJours; $i++) {
        if ($i < 10) {
            echo"<td>0" .($i) ."</td>";
        } else {
            echo"<td>".$i . " </td> ";
        }
        if (($décalage + $i) % 7 == 0) {
            echo"</tr> ";
        }
    }
}

function estBissextile($année) {
    return ($année % 4 == 0) && ($année % 100 != 0 || $année % 400 == 0);
}

function nombreJours($mois, $année) {
    if ($mois < 1 || $mois > 12) {
        throw new Exception('les mois sont incohérent; ' . $mois);

    }
    $nbJours = 31;

    if ($mois == 4 || $mois == 6 || $mois == 9 || $mois == 11) {
        $nbJours = 30;

    } else if ($mois == 2) {
        if (estBissextile($année)) {
            $nbJours = 29;
        } else {
            $nbJours = 28;
        }
    }
    return $nbJours;
}

function numéroJour($jour, $mois, $année) {
    if ($mois < 1 || $mois > 12) {
        throw new Exception('Numéro du mois incohérent ' . $mois);
    }

    if ($jour < 1 || $jour > nombreJours($mois, $année)) {
        throw new Exception('Numéro du jour impossible pour ce mois ' . $jour . " " . $mois);
    }
    $h = 0;
    $nouvelAnnée = $année;
    $jour = 1;
    $q = $jour;
    $m = $mois;

    if ($mois == 1 || $mois == 2) {
        $nouvelAnnée = $année - 1;
        $m = $mois + 12;
    }
    $j = intdiv($nouvelAnnée , 100);
    $k = $nouvelAnnée % 100;
    $h = (($q + intdiv(((($m + 1) * 13)) , 5)+ ($k) + intdiv(($k) , 4) + intdiv(($j) , 4) + (5 * ($j)) + 5) % 7);
    return $h;
}

echo "<table border=1>";
$mois = isset($_GET["mois"])?$_GET["mois"]:8;
$année =isset($_GET["annee"])?$_GET["annee"]:2001;
afficherTitre($mois, $année);
afficherEntête();
$nbJours = nombreJours($mois, $année);
$décalage = numéroJour($nbJours, $mois, $année);
afficherMois($décalage, $nbJours);
echo "</table>";

?>

<style>
    th{
        padding : 10px;
        color : red;
        font-size: 18px;

    }
    td{
        padding : 10px;
        color : red;
        background-color: aliceblue;
    }
</style>