<?php
require 'Model/Message.php';
require 'Model/content.php';

function allMessages() {
    // Pas de paramètre supplémentaire ici
    $message = new Message();
    $allMessages = $message->getAllMessages();
    require "View/AllMessages.php";
}

function content($id){
    $content = new content();
    $allContent =$content->getContent($id);
    require "View/AllContent.php";
}