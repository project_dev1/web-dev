<?php

abstract class Model
{
    protected function executeRequest($sql, $params = null)
    {
        $db = new PDO("mysql:host=localhost;dbname=webg4;charset=utf8", "webg4", "esi123",
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        if ($params == null) {              // exécution directe
            $result = $db->query($sql);
        } else {                                    // requête préparée
            $result = $db->prepare($sql);
            $result->execute($params);
        }
        return $result;
    }

    protected function executeRequest1($sql, $params = null)
    {
        $pdo = new PDO("mysql:host=localhost;dbname=webg4;charset=utf8", "webg4", "esi123",
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        if ($params == null) {
            $result = $pdo->query($sql);
        } else {
            $result = $pdo->prepare($sql);
            $result->execute($params);
        }
        return $result;
    }
}
