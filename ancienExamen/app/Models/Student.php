<?php


namespace App\Models;

use Illuminate\Support\Facades\DB;

class Student
{


    public static function getStudents()
    {
        return DB::select("SELECT student.id,student.name,sum(course.credits) credits
        From student student
        Join program program On student.id= program.student
        Join course course ON program.course  = course.id
        Group by student.name,student.id");
    }

    public static function getInfoStudents($idStudent){
        return DB::select('select course.id idCourse, course.title, course.credits
        FROM program
        JOIN course ON program.course = course.id
        where program.student = ?', [$idStudent]);
    }

}
