<?php


namespace App\Http\Controllers;

use App\Models\Student;


class StudentCtrl extends Controller
{

    public static function getStudent()
    {
        $students = Student::getStudents();
        return response()->json($students);
    }

    public static function getStudents()
    {
        $students = Student::getStudents();
        return view("accueil", compact("students"));
    }

    public static function getInfoStudents($idStudent)
    {
        $info = Student::getInfoStudents($idStudent);
        return response()->json($info);
    }

}
