@extends('canvas')

@section('content')
    <style>
        body {
            display: flex;
            color:Red;
        }
    </style>


<select>
    <option>Veuillez choisir un etudiant</option>
@foreach($students as $stud)
        <option value="{{ $stud->id }}">{{$stud->name}} - {{$stud->credits }} crédits</option>
    @endforeach
</select>
    <table id ="pae"></table>
    <script>
        $('select').on('change',function (){
            $("#pae").empty();
            $("#pae").append($("<tr>")
                .append($("<th>").text("Sigle"))
                .append($("<th>").text("Titre"))
                .append($("<th>").text("Credits")));
            $.getJSON("/api/pae/student/" + this.value,(data)=>{
            data.forEach(e=> {
                $("#pae").append($("<tr>")
                    .append($("<td>").text(e.idCourse))
                    .append($("<td>").text(e.title))
                    .append($("<td>").text(e.credits)));
                })
            })
        });
    </script>
@endsection
