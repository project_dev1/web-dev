@extends('canvas')

@section('content')

    <!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Liste des séances</title>
</head>
<body>
<header>
    <!--<img id="logo" src="css/he2b-esi.jpg" alt="HE2B-ESI">-->
    <h1>WEB II - Cinema</h1>
</header>
<main>
    <h1>Liste des seances</h1>
    <table id="table">
        <tr>
            <th>Jour</th>
            <th>Heure</th>
            <th>Film</th>
            <th>Salle</th>
            <th>Places disponible</th>
            <th>Billeteries</th>
        </tr>
        @foreach($seances as $cine)
            <tr>
                <td>{{substr($cine->start,0,10)}}</td>
                <td>{{substr($cine->start,11,16)}}</td>
                <td>{{ $cine->title }}</td>
                <td>{{ $cine->idRoom }}</td>
                <td>{{ $cine->capacity }}</td>
                <td>
                    <form action="/seance/Ticket" method="post">
                        <input type="hidden" name="id"  value="{{$cine->idRoom}}">
                        <input type="number" name="ticket">
                        <button>Acheter</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</main>
</body>
</html>

@endsection
