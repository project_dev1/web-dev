<?php


namespace App\Http\Controllers;
use App\Models\Cinema;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class CinemaCtrl extends Controller
{

     static function getSeance(){
        $seances = Cinema::getCinema();
        return response()->json($seances);
    }
    static function getSeances(){
        $seances= Cinema::getCinema();
        return view("accueil", compact("seances"));
    }
    static function updateTicket($id,$ticket){
        $seances= Cinema::updateTicket($id,$ticket);
        return view("accueil", compact("seances"));
    }
}
