<?php


namespace App\Models;

use Illuminate\Support\Facades\DB;

class Chat
{
    public static function getChannels()
    {
        return DB::select("SELECT id, name, topic FROM channels");
    }

    public static function getMessages($chatRoom)
    {
        return DB::select("SELECT messages.id, added_on, content, author_id, chan_id, chatusers.login, chatusers.displayName
        FROM messages
        JOIN chatusers ON chatusers.id = author_id
            WHERE chan_id = ?", [$chatRoom]);
    }

    public static function getPostMessage($chatRoom, $login, $content)
    {
        $user = Db::selectOne("Select id from chatusers Where login =? ", [$login]);
        return DB::insert("Insert into messages (chan_id,author_id,content)values(?,?,?)", [$chatRoom, $user->id, $content]);
    }

    public static function getUsers()
    {
        return DB::select("SELECT login, displayName FROM chatusers");
    }

    public static function getDisplayName($login)
    {
        return DB::selectOne("SELECT displayName FROM chatusers WHERE login = ?", [$login])->displayName;
    }
}
