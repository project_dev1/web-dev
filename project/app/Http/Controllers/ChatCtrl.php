<?php


namespace App\Http\Controllers;

use App\Models\Chat;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class ChatCtrl extends Controller
{

    public function getChannels()
    {
        $channels = Chat::getChannels();
        return response()->json($channels);
    }

    public function messages($chatRoom)
    {
        $messages = Chat::getMessages($chatRoom);
        return response()->json($messages);
    }

    public function postMsg($chatRoom, Request $request)
    {
        $messages = Chat::getPostMessage($chatRoom,$request["login"],$request["content"]);
        return response()->json($messages);
    }

    public function index(){
        $users= Chat::getUsers();
        return view ("accueil",["users"=>$users]);
    }

    public function showMessages($idRoom) {
        return view("chat", ["idRoom" => $idRoom]);
    }

    public function login(Request $request) {
        setcookie("pseudo", $request->login);
        return redirect()->back();
    }
}
