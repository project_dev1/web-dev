<?php
if(!empty($_GET["ue"])) {
    $ue = $_GET["ue"];
    $ues = [
        "webg2" => (object)["ects" => 5,
            "aas" => ["Développement WEB I"]],
        "webg4" => (object)["ects" => 4,
            "aas" => ["Développement WEB II", "Ergonomie"]],
        "webg5" => (object)["ects" => 3,
            "aas" => ["Développement WEB III"]]
    ];
    echo json_encode($ues[$ue]);
    exit();
}

?>

<input type="text" id="ue" placeholder="UE">
<button>Détail</button>
<table id="horaire">
    <tr>
        <th>ECTS</th>
        <td><span id="ects"></span></td>
    </tr>
    <tr>
        <th>AAs</th>
        <td><span id="aas"></span></td>
    </tr>
</table>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

<script>
    $(document).ready(function () {
        $("button").click(function () {
            let ue = $("#ue").val();
            $.get("Exempl4.php?ue=" + ue, function (jsData, status) {
                console.log(jsData);
                let ue = JSON.parse(jsData);
                $("#ects").text(ue.ects);
                $("#aas").text(ue.aas);
            });
        });
    });
</script>