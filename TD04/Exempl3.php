<?php

if(!empty($_GET["ue"])) {
    $ue = $_GET["ue"];
    $ues = [
        "webg2" => (object)["ects" => 5,
            "aas" => ["Développement WEB I"]],
        "webg4" => (object)["ects" => 4,
            "aas" => ["Développement WEB II", "Ergonomie"]],
        "webg5" => (object)["ects" => 3,
            "aas" => ["Développement WEB III"]]
    ];
    echo "
    <table>
    <tr><th>ECTS</th><td>".$ues[$ue]->ects."</td></tr>
<tr><th>AAs</th><td>".implode(', ',$ues[$ue]->aas)."</td></tr>
</table>";
    exit();
}

?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<input type="text" id="ue" placeholder="UE">
<button>Détail</button>
<div id="answer"></div>
<script>
    $(document).ready(function(){
        $("button").click(function(){
            let ue = $("#ue").val();
            $.get("Exempl3.php?ue="+ue, function(data, status){
                $("#answer").html(data);
            });
        });
    });
</script>
</body>
</html>