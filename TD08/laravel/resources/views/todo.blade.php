<?php


class todo
{

}

?> <!DOCTYPE html>
<html lang="fr">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<head><title>TODO</title></head>
<body>
<h1>À faire...</h1>
@if (count($todos) === 0)
    <p>Rien pour le moment ;)</p>
@else
    <ul>
        @foreach ($todos as $todo)
            <li>{{$todo}}</li>
        @endforeach </ul>
@endif
</body>
</html>
