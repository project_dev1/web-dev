<?php

namespace App\Http\Controllers;
use App\Models\Todo;
class TodoCtrl extends Controller
{
    public function index()
    {
        // Plus tard, on ira chercher l'info dans une BD
        $result =Todo::findAll();
        return view('todo', ['todos' => $result]);
    }
}
