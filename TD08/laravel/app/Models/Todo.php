<?php


namespace App\Models;

use PDO;
use \Illuminate\Support\Facades\DB;


class Todo
{
    public static function findAll()
    {
        $pdo = \Illuminate\Support\Facades\DB::getPdo();
        $todos = $pdo->query("select name from todos")->fetchAll(PDO::FETCH_COLUMN);
        $result = DB::select("select name from todos");
        DB::insert("INSERT INTO todos (name, description) VALUES (?, ?)",
            ['dylan bg ', 'aucune description']);

        return $todos;
    }
}

